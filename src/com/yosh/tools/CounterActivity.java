package com.yosh.tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.yosh.estadigrafos.MainActivity;
import com.yosh.estadigrafos.R;

public class CounterActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_counter);
		
		// My code
		ImageButton btnContar = (ImageButton) findViewById(R.id.ibtnContar);
		Button btnReiniciar = (Button) findViewById(R.id.btnReset);
		final TextView msgContador = (TextView) findViewById(R.id.textView1);
		
		// Cargando la fuente ttf
		Typeface fuente_ttf = Typeface.createFromAsset(getAssets(), "fonts/LCDMono.TTF");
		// Cambiando la fuente del tablero
		msgContador.setTypeface(fuente_ttf);
				
		btnReiniciar.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
			// TODO Auto-generated method stub
				msgContador.setText("000");
			}
					
		});
				
		btnContar.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				int cont = Integer.parseInt(msgContador.getText().toString());
				cont ++;
				if(cont < 10){
					msgContador.setText("00"+String.valueOf(cont));
				}else if(cont < 100){
					msgContador.setText("0"+String.valueOf(cont));
				}else{
					msgContador.setText(String.valueOf(cont));
				}
			}});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_basics, menu);
		return true;
	}
	@SuppressWarnings("deprecation")
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent intentPreferencias = new Intent(CounterActivity.this, MainActivity.class);
        	startActivity(intentPreferencias);
			return true;
		case R.id.menu_about:
			showDialog(1);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	@Override
    protected Dialog onCreateDialog(int id) {
    	Dialog dialogo = null;
    	
    	dialogo = crearDialogoAlerta();
    	
    	return dialogo;
	}
	private Dialog crearDialogoAlerta(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
     
        builder.setTitle("Acerca de");
        builder.setMessage("Andrés Aguilar\n" +
        		"andresyoshimar@gmail.com\n\n"+
        		"Todos los derechos reservados \n \t\t\t\t\t2013");
        
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}});
        return builder.create();
    }

}
