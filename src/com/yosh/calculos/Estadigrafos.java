package com.yosh.calculos;

/*
 * Archivo:		Estadigrafos.java
 * Autor:		Andres Aguilar - andresyoshimar@gmail.com
 * Fecha: 		01/02/2013 
 */
public class Estadigrafos {
	public double calculaSuma(double []elements){
		double suma = 0.0;
		
		for(double x : elements){
			suma += x;
		}
		
		return suma;
	}
	public double calculaMedia(double []elements){
		double promedio = 0.0;
		
		promedio = calculaSuma(elements)/(elements.length);
		
		return promedio;
	}
	public double calculaMediaGeometrica(double []elements){
		double mgeometrica = 1.0;
		
		for(double x : elements){
			mgeometrica *= x;
		}
		
		mgeometrica = Math.pow(mgeometrica,(1.0/elements.length));
		
		return mgeometrica;
	}
	public double calculaRango(double []elements){
		int i,j,n = elements.length;
		double t;
		 
		for(i=0; i<n-1; i++){
			for(j=0; j<n-1; j++){
				if(elements[j] > elements[j+1]){
					t=elements[j];
					elements[j]=elements[j+1];
					elements[j+1]=t;
				}
			}
		}
		
		// Obtener el mayor(último) y el menor(primero)
		double mayor = elements[(elements.length-1)];
		double menor = elements[0];
		
		double rango = mayor - menor;
		return rango;
	}
	public double calculaVarianza(double []elements){
		double media = calculaMedia(elements);
		double varianza,temp = 0.0;
		
		for(double i : elements){
			temp += Math.pow((i - media),2);
		}
		varianza = temp/(elements.length-1);
		
		return varianza;
	}
	public double calculaDesviacion(double []elements){
		double varianza = calculaVarianza(elements);
		
		return Math.sqrt(varianza);
	}
	public String elementosDescartados(double []elements, double ndesv){
		double desv = calculaDesviacion(elements);
		double media = calculaMedia(elements);
		double min,max;
		int contador = 0;
		String descartados = "";
		
		min = media - desv * ndesv;
		max = media + desv * ndesv;
		
		// cálculo
		for(double i : elements){
			if(i < min || i > max){
				descartados += String.valueOf(i) + ",";
				contador ++;
			}
		}
		descartados = "Descartados: " + contador + "\n" + descartados;
		return descartados;
	}
	public double calculaPonderada(double []elementos, double []ponderacion){
		double res = 0,sumatoria = 0;
		
		for(int x=0; x < elementos.length; x++){
			res += elementos[x] * ponderacion[x];
		}
		for(double x : ponderacion){
			sumatoria += x;
		}
		
		return (res/sumatoria);
		
	}
}
