package com.yosh.estadigrafos;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BasicsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_basics);
		
		// My code
		final Button btnCalcular = (Button) findViewById(R.id.btnCalcular);
		final EditText txtElementos = (EditText) findViewById(R.id.tfElementos);
		final EditText ndesv = (EditText) findViewById(R.id.etNDesv);
		
		btnCalcular.setOnClickListener( new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// Primero obtenemos el String que introdujo el usuario
				final String strElementos = txtElementos.getText().toString();
				final String []arrElementos = strElementos.split(",");
				final double []arrNumeros = new double[arrElementos.length];
				final String desviaciones = ndesv.getText().toString();
				double desvest = 1;
				
				boolean bandera = false;
				
				// Convertir el String en numeros (double)
				for(int x=0; x<arrElementos.length;x++){
					try{
						arrNumeros[x] = Double.parseDouble(arrElementos[x]);
						desvest = Double.parseDouble(desviaciones);
					}catch(NumberFormatException e){
						Toast.makeText(BasicsActivity.this, R.string.LlenarCampos, Toast.LENGTH_SHORT).show();
						bandera = true;
						break;
					}
				}
				
				// LLamamos al intent al hacer click
				if(!bandera){
					Intent intentCalcular = new Intent(BasicsActivity.this, ResultsActivity.class);
					Bundle bundle = new Bundle();
					
					bundle.putDoubleArray("ELEMENTOS", arrNumeros);
					bundle.putDouble("DESV", desvest);
					intentCalcular.putExtras(bundle);
					
					startActivity(intentCalcular);
				}
			}
			
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_basics, menu);
		return true;
	}
	@SuppressWarnings("deprecation")
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent intentPreferencias = new Intent(BasicsActivity.this, MainActivity.class);
        	startActivity(intentPreferencias);
			return true;
		case R.id.menu_about:
			showDialog(1);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	@Override
    protected Dialog onCreateDialog(int id) {
    	Dialog dialogo = null;
    	
    	dialogo = crearDialogoAlerta();
    	
    	return dialogo;
	}
	private Dialog crearDialogoAlerta(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
     
        builder.setTitle("Acerca de");
        builder.setMessage("Andrés Aguilar\n" +
        		"andresyoshimar@gmail.com\n\n"+
        		"Todos los derechos reservados \n \t\t\t\t\t2013");
        
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}});
        return builder.create();
    }
}
