package com.yosh.estadigrafos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.yosh.calculos.Estadigrafos;

@SuppressWarnings("deprecation")
public class ResultsActivity extends Activity {

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_results);
		
		// My code
		//Recuperamos los parametros pasados
		Bundle bundle = getIntent().getExtras();
		double []elementos = bundle.getDoubleArray("ELEMENTOS");
		double ndesv = bundle.getDouble("DESV");
				
		// Trabajamos con los elementos del arreglo
		Estadigrafos est = new Estadigrafos();
				
		// Calculamos los estadigrafos
		final double suma = est.calculaSuma(elementos);
		final double media = est.calculaMedia(elementos);
		final double geometrica = est.calculaMediaGeometrica(elementos);
		final double rango = est.calculaRango(elementos);
		final double varianza = est.calculaVarianza(elementos);
		final double desvEst = est.calculaDesviacion(elementos);
		final String descartados = est.elementosDescartados(elementos, ndesv);
				
		// Obtenemos los objetos para poner los mensajes
		final EditText etResultados = (EditText) findViewById(R.id.etResultados);
		final EditText etConjunto = (EditText) findViewById(R.id.etConjunto);
		
		// Colocamos el conjunto pasado como parámetro
		String conj = "";
		for(Double x : elementos){
			conj += String.valueOf(x) +",";
		}
		
		etConjunto.setText(conj);
		
		// HAcemos un String con el texto a presentar
		String datos = "Suma: "+ String.valueOf(suma) + "\n" +
		"Media: " + String.valueOf(media) + "\n" +
		"M. Geométrica: " + String.valueOf(geometrica) + "\n" +
		"Rango: " + String.valueOf(rango) + "\n" +
		"Varianza: " + String.valueOf(varianza) + "\n" +
		"Des estándar: " + String.valueOf(desvEst) + "\n" +
		"------------------\n" +
		"No. Desv: " + String.valueOf(ndesv) + "\n" +
		descartados;
			
		// Ponemos los resultados en pantalla
		etResultados.setText(datos);
		
		// Accion del botón al hacr click
		final Button btnCopy = (Button) findViewById(R.id.button1);
		
		btnCopy.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String text = etResultados.getEditableText().toString();
				copiarAlPortapapeles(text);
				Toast.makeText(ResultsActivity.this,"Copiado!", Toast.LENGTH_SHORT).show();
			}});
	}
	
	private void copiarAlPortapapeles(String text){
		ClipboardManager ClipMan = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
	    ClipMan.setText(text);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_basics, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent intentPreferencias = new Intent(ResultsActivity.this, MainActivity.class);
        	startActivity(intentPreferencias);
			return true;
		case R.id.menu_about:
			showDialog(1);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	@Override
    protected Dialog onCreateDialog(int id) {
    	Dialog dialogo = null;
    	
    	dialogo = crearDialogoAlerta();
    	
    	return dialogo;
	}
	private Dialog crearDialogoAlerta(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
     
        builder.setTitle("Acerca de");
        builder.setMessage("Andrés Aguilar\n" +
        		"andresyoshimar@gmail.com\n\n"+
        		"Todos los derechos reservados \n \t\t\t\t\t2013");
        
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}});
        return builder.create();
    }
}
