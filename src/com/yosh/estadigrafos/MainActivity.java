package com.yosh.estadigrafos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import com.yosh.tools.CounterActivity;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// My codde
		ImageButton ibBasicos = (ImageButton) findViewById(R.id.imageButton1);
		ImageButton ibComplejos = (ImageButton) findViewById(R.id.imageButton2);
		ImageButton ibContador = (ImageButton) findViewById(R.id.imageButton3);
		
		// Método que lanza el contador
		ibContador.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
			   //Toast.makeText(SettingsActivity.this,"Calculos basicos!", Toast.LENGTH_SHORT).show();
				Intent principal = new Intent(MainActivity.this, CounterActivity.class);
				startActivity(principal);
			}
		});
		
		// Metodo que lanza los calculos básicos
		ibBasicos.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
			   //Toast.makeText(SettingsActivity.this,"Calculos basicos!", Toast.LENGTH_SHORT).show();
				Intent principal = new Intent(MainActivity.this, BasicsActivity.class);
				startActivity(principal);
			}
		});
		ibComplejos.setOnClickListener(new OnClickListener() {
			 
			@Override
			public void onClick(View arg0) {
				Intent calculos = new Intent(MainActivity.this, ComplexActivity.class);
				startActivity(calculos);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
		case R.id.menu_about:
			showDialog(1);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	@Override
    protected Dialog onCreateDialog(int id) {
    	Dialog dialogo = null;
    	
    	dialogo = crearDialogoAlerta();
    	
    	return dialogo;
	}
	private Dialog crearDialogoAlerta(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
     
        builder.setTitle("Acerca de");
        builder.setMessage("Andrés Aguilar\n" +
        		"andresyoshimar@gmail.com\n\n"+
        		"Todos los derechos reservados \n \t\t\t\t\t2013");
        
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}});
        return builder.create();
    }
}
