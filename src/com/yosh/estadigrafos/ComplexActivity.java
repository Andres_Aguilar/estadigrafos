package com.yosh.estadigrafos;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ComplexActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_complex);
		
		final Button btnCalcular = (Button) findViewById(R.id.btnCalcular2);
		final EditText etElementos = (EditText) findViewById(R.id.etElementos);
		final EditText etPeso = (EditText) findViewById(R.id.etPeso);
		final EditText ndesv = (EditText) findViewById(R.id.etNDesv);
		
		btnCalcular.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Toast.makeText(ComplexActivity.this, "Proximamente! =P", Toast.LENGTH_SHORT).show();
				String strElementos,strPeso;
				double []dPeso = null,dElementos = null;
				double desv = 1;
				
				strElementos = etElementos.getEditableText().toString();
				strPeso = etPeso.getEditableText().toString();
				String strNdesv = ndesv.getEditableText().toString();
					
				String []Elementos = strElementos.split(",");
				String []Peso = strPeso.split(",");
					
				dElementos = new double[Elementos.length];
				dPeso = new double[Peso.length];
					
				try{
					// Convertimos todos los elementos
					for(int x=0; x < Elementos.length; x++){
						dElementos[x] = Double.parseDouble(Elementos[x]);
					}
					// Convertimos las ponderaciones
					for(int x=0; x < Peso.length; x++){
						dPeso[x] = Double.parseDouble(Peso[x]);
					}
					
					// canvertimos ndesv a double
					desv = Double.parseDouble(strNdesv);
					
					// Verificamos si el numero de elementos coincide
					if(dElementos.length == dPeso.length){
						// Procedemos a verificar las restricciones
						double suma = 0;
						for(double x : dPeso){
							suma += x;
						}
						if(suma == dElementos.length){
							// Si cumple con la restricción hacemos el calculo normal
							lanzarResultActivity(dElementos, dPeso, desv);
						}else{
							// Si no cumple, hacemos una regla de 3
							int n = dElementos.length;
							
							for(int x =0; x < dPeso.length; x++){
								dPeso[x] = (dPeso[x] * n)/suma;
							}
							// Mandamos a realizar los calculos
							lanzarResultActivity(dElementos, dPeso, desv);
						}
					}else{
						Toast.makeText(ComplexActivity.this, R.string.NoElementos, Toast.LENGTH_SHORT).show();
					}
				}catch(NumberFormatException e){
					Toast.makeText(ComplexActivity.this, R.string.LlenarCampos, Toast.LENGTH_SHORT).show();
				}
			}	
		});
	}
	public void lanzarResultActivity(double []elementos, double []ponderacion, double desv){
		Intent intentCalcular2 = new Intent(ComplexActivity.this, ResultsActivity2.class);
		
		// Mandamos los valores
		Bundle bundle = new Bundle();
		bundle.putDoubleArray("ELEMENTOS", elementos);
		bundle.putDoubleArray("PONDERACION", ponderacion);
		bundle.putDouble("DESV", desv);
		intentCalcular2.putExtras(bundle);
		
    	startActivity(intentCalcular2);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_basics, menu);
		return true;
	}
	@SuppressWarnings("deprecation")
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent intentPreferencias = new Intent(ComplexActivity.this, MainActivity.class);
        	startActivity(intentPreferencias);
			return true;
		case R.id.menu_about:
			showDialog(1);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	@Override
    protected Dialog onCreateDialog(int id) {
    	Dialog dialogo = null;
    	
    	dialogo = crearDialogoAlerta();
    	
    	return dialogo;
	}
	private Dialog crearDialogoAlerta(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
     
        builder.setTitle("Acerca de");
        builder.setMessage("Andrés Aguilar\n" +
        		"andresyoshimar@gmail.com\n\n"+
        		"Todos los derechos reservados \n \t\t\t\t\t2013");
        
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}});
        return builder.create();
    }
}
